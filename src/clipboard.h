#ifndef __RESAPPLET_H__
#define __RESAPPLET_H__

#define CLIPBOARD_NAME		"RESAPPLET_SELECTION"

gboolean resapplet_get_clipboard (void);

#endif	/* __RESAPPLET_H__ */
