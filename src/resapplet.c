/*
 * resapplet.c - The Resapplet
 *
 * A notification area applet to switch the display resolution with the
 * Xrandr extension.
 *
 * There is very little original code here.  This code was mainly
 * snipped from the netapplet by Joe Shaw and Robert Love, and from the
 * Xrandr GNOME control center capplet, from authors unknown.
 *
 * Nat Friedman <nat@novell.com>
 * Robert Love <rml@novell.com>
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <libintl.h>
#include <gdk/gdkx.h>
#include <gnome.h>
#include <X11/extensions/Xrandr.h>
#include <gconf/gconf-client.h>
#include <gtk/gtk.h>

#include "clipboard.h"
#include "eggtrayicon.h"

#define REVERT_COUNT	20
#define YAST2_ARGV { BINDIR "/gnomesu", "/sbin/yast2", "x11", NULL }

typedef struct {
	int current_width;
	int current_height;
	SizeID current_size;
	short current_rate;
	Rotation current_rotation;

	SizeID old_size;
	short old_rate;
	Rotation old_rotation;
  
	XRRScreenConfiguration *config;
	XRRScreenSize *sizes;
	int n_sizes;
	Rotation rotations;

	int user_set_resno;
	short user_set_rate;
	Rotation user_set_rotation;
} ScreenInfo;

typedef struct {
	int n_screens;
	ScreenInfo *screens;
} DisplayInfo;

typedef struct {
	unsigned int time;
	GtkLabel *label;
	GtkDialog *dialog;
	gboolean timed_out;
} TimeoutData;

typedef struct {
	DisplayInfo *di;
	
	EggTrayIcon *tray_icon;	
	GtkWidget *menu_bar;
	GtkWidget *event_box;
	GtkWidget *icon;
	GtkTooltips *tooltips;
	GtkWidget *popup_menu;
	gboolean ignore_menus;
	int last_panel_height;

	TimeoutData timeout_data;
} Resapplet;

enum {
	CHANGED_RESOLUTION,
	CHANGED_ROTATION,
	CHANGED_RATE
};

static Resapplet *resapplet;

static void populate_popup_menu (void);
static char *get_str_for_res (int width, int height, Rotation rotation);

static const char *
depth_to_string (guint depth)
{
	switch (depth) {
	case 8:
		return _("hundreds of colors");
	case 16:
		return _("thousands of colors");
	case 24:
		return _("millions of colors");
	case 32:
		return _("billions of colors");
	default:
		return _("unknown colors");
	}
}

static void
resapplet_set_tooltip (void)
{
	ScreenInfo *current_screen;
	GdkWindow *root;
	char *tip;
	gint gdk_depth;
	guint depth;
	char *resstr;
	
	if (resapplet->tooltips == NULL)
		resapplet->tooltips = gtk_tooltips_new ();

	current_screen = & resapplet->di->screens [gdk_screen_get_number (gtk_widget_get_screen (resapplet->menu_bar))];

	root = gtk_widget_get_root_window (resapplet->menu_bar);
	gdk_window_get_geometry (root, NULL, NULL, NULL, NULL, &gdk_depth);

	if (gdk_depth < 0)
		return;
	depth = (guint) gdk_depth;

	resstr = get_str_for_res (current_screen->current_width,
		current_screen->current_height,
		current_screen->current_rotation);

	tip = g_strdup_printf (_("Display information: %s with %s at %dHz"),
		               resstr,
			       depth_to_string (depth),
			       current_screen->current_rate);

	g_free (resstr);

	gtk_tooltips_set_tip (resapplet->tooltips, resapplet->event_box, tip,
			      NULL);

	g_free (tip);
}

static void
load_screen_info (GdkDisplay *display,
		  GdkScreen *screen,
		  ScreenInfo *screen_info)
{
	GdkWindow *root_window;

	root_window = gdk_screen_get_root_window (screen);
	screen_info->config = XRRGetScreenInfo (
		gdk_x11_display_get_xdisplay (display),
		gdk_x11_drawable_get_xid (GDK_DRAWABLE (root_window)));
	
	screen_info->current_rate = XRRConfigCurrentRate (screen_info->config);
	screen_info->current_size = XRRConfigCurrentConfiguration (
		screen_info->config, &screen_info->current_rotation);
	screen_info->sizes = XRRConfigSizes (screen_info->config,
					     &screen_info->n_sizes);
	screen_info->rotations = XRRConfigRotations(screen_info->config,
						    &screen_info->current_rotation);
	
	screen_info->current_width = screen_info->sizes [screen_info->current_size].width;
	screen_info->current_height = screen_info->sizes [screen_info->current_size].height;
}

static void
screen_size_changed_cb (GdkScreen *screen)
{
	GdkDisplay *display = gdk_display_get_default ();
	ScreenInfo *screen_info;
	int i;
	
	g_assert (resapplet->di->n_screens ==
		  gdk_display_get_n_screens (display));
       
	for (i = 0; i < resapplet->di->n_screens; i++) {
		screen_info = &resapplet->di->screens[i];
		if (screen == gdk_display_get_screen (display, i))
			load_screen_info (display, screen, screen_info);
	}

	populate_popup_menu ();
}

static DisplayInfo *
read_display_info (GdkDisplay *display)
{
	DisplayInfo *info;
	ScreenInfo *screen_info;
	GdkScreen *screen;
	int i;

	info = g_new (DisplayInfo, 1);
	info->n_screens = gdk_display_get_n_screens (display);
	info->screens = g_new (ScreenInfo, info->n_screens);

	for (i = 0; i < info->n_screens; i++) {
		screen = gdk_display_get_screen (display, i);
		screen_info = &info->screens[i];
		load_screen_info (display, screen, screen_info);

		g_signal_connect (screen, "size-changed",
				  G_CALLBACK (screen_size_changed_cb), NULL);
	}

	return info;
}

static void
update_display_info (DisplayInfo *info, GdkDisplay *display)
{
	ScreenInfo *screen_info;
	GdkScreen *screen;
	int i;

	g_assert (info->n_screens == gdk_display_get_n_screens (display));
  
	for (i = 0; i < info->n_screens; i++) {
		screen = gdk_display_get_screen (display, i);
      
		screen_info = &info->screens[i];

		screen_info->old_rate = screen_info->current_rate;
		screen_info->old_size = screen_info->current_size;
		screen_info->old_rotation = screen_info->current_rotation;
      
		load_screen_info (display, screen, screen_info);
	}
}

static int
get_current_resolution (ScreenInfo *screen_info)
{
	return screen_info->user_set_resno;
}

static int
get_current_rate (ScreenInfo *screen_info)
{
	return screen_info->user_set_rate;
}

static Rotation
get_current_rotation (ScreenInfo *screen_info)
{
	return screen_info->user_set_rotation;
}

static gboolean
apply_config (DisplayInfo *info)
{
	GdkDisplay *display;
	Display *xdisplay;
	gboolean changed;
	int i;

	display = gdk_display_get_default ();
	xdisplay = gdk_x11_display_get_xdisplay (display);

	changed = FALSE;
	for (i = 0; i < info->n_screens; i++) {
		ScreenInfo *screen_info = &info->screens[i];
		Status status;
		GdkWindow *root_window;
		GdkScreen *screen;
		int new_res;
		short new_rate;
		Rotation new_rotation;

		screen = gdk_display_get_screen (display, i);
		root_window = gdk_screen_get_root_window (screen);

		new_res = screen_info->user_set_resno;
		new_rate = screen_info->user_set_rate;
		new_rotation = screen_info->user_set_rotation;

		if (new_res != screen_info->current_size ||
			new_rate != screen_info->current_rate ||
			new_rotation != screen_info->current_rotation) {
			changed = TRUE;

			/* only set the rate if it looks sane */
			if (new_rate > 0 && new_rate < 120)
				status = XRRSetScreenConfigAndRate (
					xdisplay,
					screen_info->config,
					gdk_x11_drawable_get_xid (GDK_DRAWABLE (root_window)),
					new_res,
					new_rotation,
					new_rate,
					GDK_CURRENT_TIME);
			else
				status = XRRSetScreenConfig (
                                	xdisplay,
	                                screen_info->config,
        	                        gdk_x11_drawable_get_xid (GDK_DRAWABLE (root_window)),
                	                new_res,
					new_rotation,
	                                GDK_CURRENT_TIME);
		}
	}

	update_display_info (info, display);
  
	/*
	 * xscreensaver should handle this itself, but does not currently so
	 * we hack it.  Ignore failures in case xscreensaver is not installed
	 */
	if (changed)
		g_spawn_command_line_async ("xscreensaver-command -restart",
					    NULL);

	return changed;
}

static int
revert_config (DisplayInfo *info)
{
	int i;
	GdkDisplay *display;
	Display *xdisplay;
	GdkScreen *screen;

	display = gdk_display_get_default ();
	xdisplay = gdk_x11_display_get_xdisplay (display);
  
	for (i = 0; i < info->n_screens; i++) {
		ScreenInfo *screen_info = &info->screens[i];
		Status status;
		GdkWindow *root_window;

		screen = gdk_display_get_screen (display, i);
		root_window = gdk_screen_get_root_window (screen);

		status = XRRSetScreenConfigAndRate (
			xdisplay,
			screen_info->config,
			gdk_x11_drawable_get_xid (GDK_DRAWABLE (root_window)),
			screen_info->old_size,
			screen_info->old_rotation,
			screen_info->old_rate,
			GDK_CURRENT_TIME);
      
	}

	update_display_info (info, display);

	/*
	 * xscreensaver should handle this itself, but does not currently so
	 * we hack it.  Ignore failures in case xscreensaver is not installed
	 */
	g_spawn_command_line_async ("xscreensaver-command -restart", NULL);

	return 0;
}

static gboolean
show_resolution (int width, int height)
{
	if (width >= 800 && height >= 600)
		return TRUE;

	if (width == 640 && height == 480)
		return TRUE;

	return FALSE;
}

static char *
timeout_string (unsigned int seconds)
{
	return g_strdup_printf (ngettext ("Testing the new settings. If you don't respond in %d second the previous settings will be restored.",
			  "Testing the new settings. If you don't respond in %d seconds the previous settings will be restored.",
		seconds), seconds);
}

static gboolean
save_timeout_callback (gpointer _data)
{
	TimeoutData *data = _data;
	char *str;
  
	data->time --;

	if (data->time == 0) {
		gtk_dialog_response (data->dialog, GTK_RESPONSE_NO);
		data->timed_out = TRUE;
		return FALSE;
	}

	str = timeout_string (data->time);
	gtk_label_set_text (data->label, str);
	g_free (str);

	return TRUE;
}

static int
run_revert_dialog (DisplayInfo *info, GtkWidget *parent, int changed)
{
	GtkWidget *dialog;
	GtkWidget *hbox;
	GtkWidget *vbox;
	GtkWidget *label;
	GtkWidget *label_sec;
	GtkWidget *image;
	int res;
	guint timeout;
	char *str;
	ScreenInfo *current_screen;
	GtkRequisition requisition;

	dialog = gtk_dialog_new ();
	gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (parent));
	gtk_window_set_destroy_with_parent (GTK_WINDOW (dialog), TRUE);
	gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
	gtk_container_set_border_width (GTK_CONTAINER (dialog), 12);
	gtk_dialog_set_has_separator (GTK_DIALOG (dialog), FALSE);

	label = gtk_label_new (NULL);
	if (changed == CHANGED_RATE)
	{
	    gtk_window_set_title (GTK_WINDOW (dialog), _ ("Keep Refresh Rate"));
	    str = g_strdup_printf ("<b>%s</b>",
				   _("Do you want to keep this refresh rate?"));
	}
	else if (changed == CHANGED_ROTATION)
	{
	    gtk_window_set_title (GTK_WINDOW (dialog), _ ("Keep Rotation"));
	    str = g_strdup_printf ("<b>%s</b>",
				   _("Do you want to keep this rotation?"));
	}
	else /* changed == CHANGED_RESOLUTION */
	{
	    gtk_window_set_title (GTK_WINDOW (dialog), _ ("Keep Resolution"));
	    str = g_strdup_printf ("<b>%s</b>",
				   _("Do you want to keep this resolution?"));
	}

	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);
	image = gtk_image_new_from_stock (GTK_STOCK_DIALOG_QUESTION,
					  GTK_ICON_SIZE_DIALOG);
	gtk_misc_set_alignment (GTK_MISC (image), 0.5, 0.0);
  
	gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
	gtk_label_set_selectable (GTK_LABEL (label), TRUE);
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);

	str = timeout_string (REVERT_COUNT);
	label_sec = gtk_label_new (str);
	g_free (str);
	gtk_label_set_line_wrap (GTK_LABEL (label_sec), TRUE);
	gtk_label_set_selectable (GTK_LABEL (label_sec), TRUE);
	gtk_misc_set_alignment (GTK_MISC (label_sec), 0.0, 0.5);

	hbox = gtk_hbox_new (FALSE, 6);
	vbox = gtk_vbox_new (FALSE, 6);

	gtk_box_pack_start (GTK_BOX (vbox), label, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), label_sec, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), vbox, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), hbox, FALSE,
			    FALSE, 0);

	if (changed == CHANGED_RATE)
	{
	    gtk_dialog_add_buttons (GTK_DIALOG (dialog),
				    _("Use _previous refresh rate"),
				    GTK_RESPONSE_NO, _("_Keep refresh rate"),
				    GTK_RESPONSE_YES, NULL);
	}
	else if (changed == CHANGED_ROTATION)
	{
	    gtk_dialog_add_buttons (GTK_DIALOG (dialog),
				    _("Use _previous rotation"),
				    GTK_RESPONSE_NO, _("_Keep rotation"),
				    GTK_RESPONSE_YES, NULL);
	}
	else /* changed == CHANGED_RESOLUTION */
	{
	    gtk_dialog_add_buttons (GTK_DIALOG (dialog),
				    _("Use _previous resolution"),
				    GTK_RESPONSE_NO, _("_Keep resolution"),
				    GTK_RESPONSE_YES, NULL);
	}


	gtk_widget_show_all (hbox);

	/*
	 * Center the dialog.  Using the window manager to center it doesn't
	 * work, since it takes metacity a few seconds to figure out where it
	 * should go.
	 */
	gtk_widget_size_request (GTK_WIDGET (dialog), &requisition);
	current_screen = & info->screens [gdk_screen_get_number (gtk_window_get_screen (GTK_WINDOW (dialog)))];
	gtk_window_move (GTK_WINDOW (dialog),
			 (current_screen->current_width / 2) - (requisition.width / 2),
			 (current_screen->current_height / 2) - (requisition.height / 2));

	resapplet->timeout_data.time      = REVERT_COUNT;
	resapplet->timeout_data.label     = GTK_LABEL (label_sec);
	resapplet->timeout_data.dialog    = GTK_DIALOG (dialog);
	resapplet->timeout_data.timed_out = FALSE;

	timeout = g_timeout_add (1000, save_timeout_callback, & (resapplet->timeout_data));

	gtk_widget_set_sensitive (resapplet->menu_bar, FALSE);

	res = gtk_dialog_run (GTK_DIALOG (dialog));

	if (! resapplet->timeout_data.timed_out)
		g_source_remove (timeout);

	gtk_widget_destroy (dialog);
	resapplet->timeout_data.dialog = NULL;
	gtk_widget_set_sensitive (resapplet->menu_bar, TRUE);
	
	return (res == GTK_RESPONSE_YES);
}

#ifdef HOST_NAME_MAX
# define HOSTNAME_LEN	(HOST_NAME_MAX + 1)
#else
# define HOSTNAME_LEN	256
#endif

static void
save_to_gconf (DisplayInfo *info, gboolean save_computer)
{
	GConfClient    *client;
	gboolean res;
	char hostname[HOSTNAME_LEN];
	char *path, *key, *str;
	int i;

	if (gethostname (hostname, sizeof (hostname)) < 0)
		return;
	/* behavior of terminating NULL is undefined if hostname is truncated */
	hostname [HOSTNAME_LEN-1] = '\0';

	client = gconf_client_get_default ();

	if (save_computer)
		path = g_strconcat ("/desktop/gnome/screen/",
				    hostname,
				    "/",
				    NULL);
	else
		path = g_strdup ("/desktop/gnome/screen/default/");
	       
	for (i = 0; i < info->n_screens; i++) {
		ScreenInfo *screen_info = &info->screens[i];
		int new_res, new_rate;
		Rotation new_rotation;

		new_res = get_current_resolution (screen_info);
		new_rate = get_current_rate (screen_info);
		new_rotation = get_current_rotation (screen_info);

		key = g_strdup_printf ("%s%d/resolution", path, i);
		str = g_strdup_printf ("%dx%d",
				       screen_info->sizes[new_res].width,
				       screen_info->sizes[new_res].height);
      
		res = gconf_client_set_string  (client, key, str, NULL);
		g_free (str);
		g_free (key);
      
		key = g_strdup_printf ("%s%d/rate", path, i);
		res = gconf_client_set_int  (client, key, new_rate, NULL);
		g_free (key);

		key = g_strdup_printf ("%s%d/rotation", path, i);
		res = gconf_client_set_int  (client, key, (int)new_rotation, NULL);
		g_free (key);
	}

	g_free (path);
	g_object_unref (client);
}

static void
quit_cb (GtkMenuItem *mi G_GNUC_UNUSED, gpointer user_data G_GNUC_UNUSED)
{
	gtk_exit (0);
}

static void
remove_old_items (gpointer node, gpointer user_data)
{
	gtk_container_remove (GTK_CONTAINER (user_data), GTK_WIDGET (node));
}

static void
menu_select_cb (GtkMenuItem *mi, gpointer user_data)
{
	ScreenInfo *si = user_data;
	int resno, rate, changed;
	Rotation rotation;

	g_assert (si);

	resno = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (mi), "resno"));
	rate = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (mi), "rate"));
	rotation = (Rotation)GPOINTER_TO_INT (g_object_get_data (G_OBJECT (mi), "rotation"));
	changed = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (mi), "changed"));

	if (resapplet->ignore_menus)
		return;

	si->user_set_resno = resno;
	si->user_set_rate = rate;
	si->user_set_rotation = rotation;

	if (apply_config (resapplet->di)) {
		if (!run_revert_dialog (resapplet->di,
				GTK_WIDGET (resapplet->tray_icon), changed)) {
			revert_config (resapplet->di);
			return;
		}
	}

	save_to_gconf (resapplet->di, FALSE);

	resapplet_set_tooltip ();
}

static int
get_rate_for_res (ScreenInfo *si, int size_nr)
{
	int nrates, closest_rate_nr, i;
	short *rates;

	rates = XRRConfigRates (si->config, size_nr, &nrates);

	/* Find the closest rate to the current rate. */
	closest_rate_nr = -1;
	for (i = 0; i < nrates; i++) {
		if ((closest_rate_nr < 0) ||
		    (ABS (rates [i] - si->current_rate) <
		     ABS (rates [closest_rate_nr] - si->current_rate)))
			closest_rate_nr = i;
	}

	return (int) rates [closest_rate_nr];
}

static char *
get_str_for_res (int width, int height, Rotation rotation)
{
	if ((rotation & RR_Rotate_90) ||
		(rotation & RR_Rotate_270))
	{
		return g_strdup_printf ("%dx%d", height, width);
	}

	return g_strdup_printf ("%dx%d", width, height);
}

static void
configure_activate_cb (GtkMenuItem *mi G_GNUC_UNUSED,
		       gpointer user_data G_GNUC_UNUSED)
{
	char *argv[] = YAST2_ARGV;
	GError *err = NULL;

	if (!g_spawn_async (NULL, argv, NULL, 0, NULL, NULL, NULL, &err)) {
		GtkWidget *dialog;

		dialog = gtk_message_dialog_new_with_markup (
			NULL, 0, GTK_MESSAGE_ERROR, GTK_BUTTONS_OK,
			"<span weight=\"bold\" size=\"larger\">"
			"Display configuration could not be run"
			"</span>\n\n"
			"%s", err->message);
		gtk_dialog_run (GTK_DIALOG (dialog));
		gtk_widget_destroy (dialog);

		g_error_free (err);
	}
}

static char *
get_rotation_menu_text (Rotation rotation)
{
	switch (rotation) {
	case RR_Rotate_90:
		return _("Counterclockwise");
	case RR_Rotate_180:
		return _("Upside down");
	case RR_Rotate_270:
		return _("Clockwise");
	default:
		return _("Normal");
	}
}

static GSList *
add_rotation_menu_item (GSList *group, ScreenInfo *si, GtkMenuShell *shell,
	Rotation rotation, Rotation current_rotation)
{
	GtkWidget *mi;
	char *text;

	text = g_strdup (get_rotation_menu_text (rotation));
	mi = gtk_radio_menu_item_new_with_label (group, text);
	g_free (text);

	group = gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (mi));
	if (rotation == current_rotation)
	{
		gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (mi), TRUE);
	}

	g_object_set_data (G_OBJECT (mi), "resno", GINT_TO_POINTER ((int) si->current_size));
	g_object_set_data (G_OBJECT (mi), "rate", GINT_TO_POINTER ((int) si->current_rate));
	g_object_set_data (G_OBJECT (mi), "rotation", GINT_TO_POINTER ((int) rotation));
	g_object_set_data (G_OBJECT (mi), "changed", GINT_TO_POINTER (CHANGED_ROTATION));

	g_signal_connect (mi, "activate", G_CALLBACK (menu_select_cb), si);

	gtk_menu_shell_append (shell, mi);

	gtk_widget_show (mi);

	return group;
}

static void
populate_popup_menu (void)
{
	GtkWidget *mi;
	GtkWidget *image;
	int i;

	g_list_foreach (GTK_MENU_SHELL (resapplet->popup_menu)->children,
			remove_old_items, resapplet->popup_menu);

	resapplet->ignore_menus = TRUE;

	for (i = 0; i < resapplet->di->n_screens; i++) {
		ScreenInfo *si = & resapplet->di->screens [i];
		Rotation rot;
		char *text;
		SizeID current_size;
		GSList *group;
		int j;
		short *rates;
		int n_rates;

		group = NULL;
		current_size = XRRConfigCurrentConfiguration (si->config, &rot);
		rates = XRRConfigRates (si->config, current_size, &n_rates);

		if ((si->n_sizes > 1 || n_rates > 1) && resapplet->di->n_screens > 1) {
			text = g_strdup_printf (_("Screen %d"), i);
			mi = gtk_menu_item_new_with_label (text);
			g_free (text);

			gtk_menu_shell_append (GTK_MENU_SHELL (resapplet->popup_menu), mi);
			gtk_widget_show (mi);
		}

		if (si->n_sizes > 1)
		{
			/* Build list of screen resolutions ... */
			mi = gtk_menu_item_new_with_label (_("Screen resolution"));

			gtk_widget_set_sensitive (mi, FALSE);
			gtk_widget_show (mi);
			gtk_menu_shell_append (GTK_MENU_SHELL (resapplet->popup_menu), mi);

			for (j = 0; j < si->n_sizes; j++) {
				int rate;

				if (!show_resolution (si->sizes [j].width,
						      si->sizes[j].height) && j != current_size)
					continue;

				rate = get_rate_for_res (si, j);

				text = get_str_for_res (si->sizes[j].width,
					si->sizes[j].height, si->current_rotation);

				mi = gtk_radio_menu_item_new_with_label (group, text);
				g_free (text);

				group = gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (mi));

				if (si->current_width == si->sizes[j].width &&
				    si->current_height == si->sizes[j].height)
					gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (mi), TRUE);

				g_object_set_data (G_OBJECT (mi), "resno", GINT_TO_POINTER (j));
				g_object_set_data (G_OBJECT (mi), "rate", GINT_TO_POINTER (rate));
				g_object_set_data (G_OBJECT (mi), "rotation", GINT_TO_POINTER ((int) si->current_rotation));
				g_object_set_data (G_OBJECT (mi), "changed", GINT_TO_POINTER (CHANGED_RESOLUTION));

				g_signal_connect (mi, "activate",
						  G_CALLBACK (menu_select_cb), si);

				gtk_menu_shell_append (GTK_MENU_SHELL (resapplet->popup_menu), mi);
				gtk_widget_show (mi);
			}
		}

		/* RR_Rotate_0 is always available.  Don't display the
		 * submenu if it's the only rotation we have. */
		if ((si->rotations & RR_Rotate_90)
		    || (si->rotations & RR_Rotate_180)
		    || (si->rotations & RR_Rotate_270)) 
		{
			GtkMenuShell *shell = GTK_MENU_SHELL (resapplet->popup_menu);
			mi = gtk_menu_item_new_with_label (_("Screen rotation"));

			gtk_widget_set_sensitive (mi, FALSE);
			gtk_widget_show (mi);
			gtk_menu_shell_append (GTK_MENU_SHELL (resapplet->popup_menu), mi);

			group = NULL;

			if (si->rotations & RR_Rotate_0)
			{
				group = add_rotation_menu_item (group, si, shell,
					RR_Rotate_0, si->current_rotation);
			}

			if (si->rotations & RR_Rotate_270)
			{
				group = add_rotation_menu_item (group, si, shell,
					RR_Rotate_270, si->current_rotation);
			}

			if (si->rotations & RR_Rotate_180)
			{
				group = add_rotation_menu_item (group, si, shell,
					RR_Rotate_180, si->current_rotation);
			}

			if (si->rotations & RR_Rotate_90)
			{
				group = add_rotation_menu_item (group, si, shell,
					RR_Rotate_90, si->current_rotation);
			}
		}

		if (n_rates > 1)
		{
			/* Build list of screen refresh rates ... */
			mi = gtk_menu_item_new_with_label (_("Screen refresh rate"));

			gtk_widget_set_sensitive (mi, FALSE);
			gtk_widget_show (mi);
			gtk_menu_shell_append (GTK_MENU_SHELL (resapplet->popup_menu), mi);

			group = NULL;
			for (j = 0; j < n_rates; j++)
			{
				text = g_strdup_printf ("%d Hz", rates[j]);
				mi = gtk_radio_menu_item_new_with_label (group, text);
				g_free (text);

				group = gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (mi));

				if (rates[j] == si->current_rate)
					gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (mi), TRUE);

				g_object_set_data (G_OBJECT (mi), "resno", GINT_TO_POINTER ((int) current_size));
				g_object_set_data (G_OBJECT (mi), "rate", GINT_TO_POINTER ((int) rates[j]));
				g_object_set_data (G_OBJECT (mi), "rotation", GINT_TO_POINTER ((int) si->current_rotation));
				g_object_set_data (G_OBJECT (mi), "changed", GINT_TO_POINTER (CHANGED_RATE));

				g_signal_connect (mi, "activate", G_CALLBACK (menu_select_cb), si);

				gtk_menu_shell_append (GTK_MENU_SHELL (resapplet->popup_menu), mi);

				gtk_widget_show (mi);
			}
		}

		mi = gtk_separator_menu_item_new ();
		gtk_widget_show (mi);
		gtk_menu_shell_append (GTK_MENU_SHELL (resapplet->popup_menu), mi);
	}

	resapplet->ignore_menus = FALSE;


	/* 'Configure Display Settings' item */
	image = gtk_image_new_from_stock (GTK_STOCK_PREFERENCES,
					  GTK_ICON_SIZE_MENU);
	mi = gtk_image_menu_item_new_with_mnemonic (_("_Configure Display Settings"));
	gtk_image_menu_item_set_image ((GtkImageMenuItem *) mi, image);	
	gtk_menu_shell_append (GTK_MENU_SHELL (resapplet->popup_menu), mi);
	gtk_widget_show (mi);
	g_signal_connect (mi, "activate", G_CALLBACK (configure_activate_cb),
			  resapplet);

	mi = gtk_separator_menu_item_new ();
	gtk_widget_show (mi);
	gtk_menu_shell_append (GTK_MENU_SHELL (resapplet->popup_menu), mi);
	
	mi = gtk_image_menu_item_new_with_mnemonic (_("_Remove From Panel"));
	image = gtk_image_new_from_stock (GTK_STOCK_REMOVE, GTK_ICON_SIZE_MENU);
	gtk_image_menu_item_set_image ((GtkImageMenuItem *) mi, image);
	gtk_menu_shell_append (GTK_MENU_SHELL (resapplet->popup_menu), mi);
	gtk_widget_show (mi);
	g_signal_connect (mi, "activate", G_CALLBACK (quit_cb), NULL);
}

static void
resapplet_set_icon (void)
{
	GdkPixbuf *pixbuf;
	int panel_w;
	int panel_h;
	int icon_size;

	gtk_window_get_size (
		GTK_WINDOW (gtk_widget_get_toplevel (resapplet->menu_bar)),
		&panel_w, &panel_h);

	if (panel_h == resapplet->last_panel_height)
		return;

	resapplet->last_panel_height = panel_h;
	
	if (panel_h < 30)
		icon_size = 16;
	else
		icon_size = 24;

	pixbuf = gtk_icon_theme_load_icon (gtk_icon_theme_get_default (),
					   "resapplet", icon_size, 0, NULL);
	/*
	 * Scale the icon rather than clip it if our allocation just isn't
	 * what we want it to be.
	 */
	if (GTK_WIDGET_REALIZED (resapplet->icon) &&
		resapplet->icon->allocation.height < icon_size) {
		int new_size = resapplet->icon->allocation.height;
		GdkPixbuf *new_pixbuf;

		new_pixbuf = gdk_pixbuf_scale_simple (pixbuf,
						      new_size, new_size,
						      GDK_INTERP_BILINEAR);
		g_object_unref (pixbuf);
		pixbuf = new_pixbuf;
	}

	if (pixbuf) {
		gtk_image_set_from_pixbuf (GTK_IMAGE (resapplet->icon), pixbuf);
		g_object_unref (pixbuf);
	}
}

static void
size_changed_cb (void)
{
	/* If the panel changed size, we might need to change the icon. */
	resapplet_set_icon ();
}

/* I hate doing this. */
static void tray_icon_destroy_cb (GtkWidget *, void *);

static void menu_show_cb (GtkWidget *menu G_GNUC_UNUSED,
			  void *data G_GNUC_UNUSED)
{
	if (resapplet->tooltips)
		gtk_tooltips_disable (resapplet->tooltips);
}

static void menu_hide_cb (GtkWidget *menu G_GNUC_UNUSED,
			  void *data G_GNUC_UNUSED)
{
	if (resapplet->tooltips)
		gtk_tooltips_enable (resapplet->tooltips);
}

static gboolean
resapplet_create_tray_icon (gpointer data)
{
	Resapplet *applet = (Resapplet *) data;
	GtkWidget *top_menu_item;
	GtkStyle  *style;
	int       i;
	const gchar *rcstyle = " \
		style \"MenuBar\" \n\
		{ \n\
			GtkMenuBar::shadow_type = none \n\
			GtkMenuBar::internal-padding = 0 \n\
		} \n\
		style \"MenuItem\" \n\
		{ \n\
			xthickness=0 \n\
			ythickness=0 \n\
		} \n\
		widget_class \"GtkMenuBar\" style \"MenuBar\"\n\
		widget \"*ToplevelMenu*\" style \"MenuItem\"\n";

	gtk_rc_parse_string (rcstyle);

	/* Event box for tooltips */
	applet->event_box = gtk_event_box_new ();
	gtk_container_set_border_width (GTK_CONTAINER (applet->event_box), 0);

	applet->tray_icon = egg_tray_icon_new ("applet");
	g_signal_connect (G_OBJECT (applet->tray_icon), "destroy",
			  G_CALLBACK (tray_icon_destroy_cb), NULL);

	applet->menu_bar = gtk_menu_bar_new ();
	style = gtk_style_new ();
	for (i = 0; i < 5; i++)
		style->bg_pixmap[i] = (GdkPixmap *) GDK_PARENT_RELATIVE;
	style->xthickness = style->ythickness = 0;

	gtk_widget_set_style (applet->menu_bar, style);

	top_menu_item = gtk_menu_item_new();
	gtk_widget_set_name (top_menu_item, "ToplevelMenu");
	gtk_container_set_border_width (GTK_CONTAINER (top_menu_item), 0);

	gtk_container_add (GTK_CONTAINER (applet->event_box),
			   applet->menu_bar);
	gtk_container_add (GTK_CONTAINER (applet->tray_icon),
			   applet->event_box);
	gtk_widget_show_all (GTK_WIDGET (applet->event_box));

	g_signal_connect (top_menu_item, "size-allocate",
			  G_CALLBACK (size_changed_cb), NULL);

	applet->icon = gtk_image_new ();
	gtk_container_add (GTK_CONTAINER (top_menu_item), applet->icon);
	gtk_widget_show (applet->icon);

	gtk_menu_shell_append (GTK_MENU_SHELL (applet->menu_bar),
			       top_menu_item);

	gtk_widget_show (top_menu_item);

	applet->popup_menu = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (top_menu_item),
				   applet->popup_menu);
	g_signal_connect (applet->popup_menu, "show",
			  G_CALLBACK (menu_show_cb), NULL);
	g_signal_connect (applet->popup_menu, "hide",
			  G_CALLBACK (menu_hide_cb), NULL);

	return FALSE;
}

static void
tray_icon_destroy_cb (GtkWidget *widget G_GNUC_UNUSED, void *data G_GNUC_UNUSED)
{
	g_return_if_fail (resapplet);
	g_return_if_fail (resapplet->tray_icon);

	g_object_unref (G_OBJECT (resapplet->tray_icon));
	resapplet->tray_icon = NULL;
	g_idle_add (resapplet_create_tray_icon, resapplet);
}

static Resapplet *
resapplet_new (void)
{
	Resapplet *applet;

	applet = g_new0 (Resapplet, 1);

	applet->di = read_display_info (gdk_display_get_default ());

	resapplet_create_tray_icon (applet);

	return applet;
}

int
main (int argc, char *argv[])
{
	GnomeClient *client;
	Display *xdisplay;
	GdkDisplay *display;
	int event_base, error_base;

#ifdef ENABLE_NLS
	const char *domaindir, *codeset, *domain;

	domaindir = bindtextdomain (GETTEXT_PACKAGE, GNOMELOCALEDIR);
	codeset = bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	domain = textdomain (GETTEXT_PACKAGE);
#endif

	gnome_program_init ("resapplet", VERSION,
			    LIBGNOMEUI_MODULE,
			    argc, argv,
			    GNOME_PROGRAM_STANDARD_PROPERTIES,
			    GNOME_CLIENT_PARAM_SM_CONNECT, TRUE,
			    NULL);

	gtk_icon_theme_append_search_path (gtk_icon_theme_get_default (),
					   ICONDIR);

	client = gnome_master_client ();
	gnome_client_set_restart_command (client, argc, argv);
	if (resapplet_get_clipboard ())
		gnome_client_set_restart_style (client,
						GNOME_RESTART_IF_RUNNING);
	else {
		/* if we are already running, silently exit */
		gnome_client_set_restart_style (client, GNOME_RESTART_NEVER);
		return 1;
	}
	
	/* Check if XRandR is supported on this particular display */
	display = gdk_display_get_default ();
	xdisplay = gdk_x11_display_get_xdisplay (display);

	if (!XRRQueryExtension (xdisplay, &event_base, &error_base))
		return 1;

	gtk_init (&argc, &argv);

	resapplet = resapplet_new ();
	resapplet_set_icon ();

	resapplet_set_tooltip ();

	populate_popup_menu ();

	gtk_widget_show (GTK_WIDGET (resapplet->tray_icon));

	gtk_main ();

	return 0;
}
